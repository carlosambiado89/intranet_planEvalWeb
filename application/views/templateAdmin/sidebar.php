<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url() ?>assets/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <i class="fa fa-circle text-success"></i> Online
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
          
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i> <span>Jardines</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
              <li><a href="<?php echo base_url()?>funcionarios/Index/registroFuncionarios"><i class="fa fa-circle-o text-aqua"></i> Registro Funcionarios</a></li>
              <li><a href="<?php echo base_url()?>funcionarios/Index/listadoFuncionarios"><i class="fa fa-circle-o text-aqua"></i> Listado Funcionarios</a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> Reporte Funcionarios</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>