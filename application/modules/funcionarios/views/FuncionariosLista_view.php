  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $tituloMantenedor; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> PlanEvalWeb</a></li>
        <li><a href="#">Funcionarios</a></li>
        <li class="active">Lista de Funcionarios</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title"><?php echo $tituloFormulario; ?></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-info btn-sm" data-widget="collapse" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body no-padding">
            <table class="table">
                <tr>
                  <th style="width: 15%">Rut</th>
                  <th style="width: 20%">Nombres</th>
                  <th style="width: 20%">Cargo</th>
                  <th style="width: 20%">F. Ingreso</th>
                  <th style="width: 5%">Ver</th>
                  <th style="width: 5%">Mod.</th>
                  <th style="width: 5%">Elim.</th>
                </tr>
                <tr>
                    <td>11.222.333.4</td>
                    <td>Marcela Durán Rojas</td>
                    <td><span class="label bg-green">Educadora de Párvulos</span></td>
                    <td>10 de Noviembre de 2010</td>
                    <td><i class="fa fa-folder-open fa-2x" aria-hidden="true"></i></td>
                    <td><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></td>
                    <td><i class='fa fa-trash-o fa-2x'></i></td>
                </tr>
                <tr>
                    <td>11.222.333.4</td>
                    <td>Marcela Durán Rojas</td>
                    <td><span class="label bg-aqua">Asistente de Párvulos</span></td>
                    <td>10 de Noviembre de 2010</td>
                    <td><i class="fa fa-folder-open fa-2x" aria-hidden="true"></i></td>
                    <td><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></td>
                    <td><i class='fa fa-trash-o fa-2x'></i></td>
                </tr>
                <tr>
                    <td>11.222.333.4</td>
                    <td>Marcela Durán Rojas</td>
                    <td><span class="label bg-purple">Directora de Jardín</span></td>
                    <td>10 de Noviembre de 2010</td>
                    <td><i class="fa fa-folder-open fa-2x" aria-hidden="true"></i></td>
                    <td><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></td>
                    <td><i class='fa fa-trash-o fa-2x'></i></td>
                </tr>
                
              </table>
                
            
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <center>
            <ul class="pagination pagination-sm no-margin">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
            </ul>
            </center>    
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>


